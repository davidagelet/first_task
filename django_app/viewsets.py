from django_app.models import Post
from django_app.serializers import PostSerializer
from rest_framework import viewsets
from rest_framework.decorators import action

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    @action(methods=['post'], detail=False)
    def dump_posts(self, request):
        return ""


